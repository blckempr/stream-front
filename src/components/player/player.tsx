import React from "react";
import { useStore } from "store/helpers";
import { observer } from "mobx-react";
import RiichiButton from "components/riichi-button/riichi-button";
import { PlayerType } from "types";
import RiichiStick from "components/riichi-stick/riichi-stick";

import styles from "components/player/player.module.css";

type Props = {
  player: PlayerType;
  isPanel?: boolean;
  rightSide?: boolean;
  riichiStick?: boolean;
  riichiBtn?: boolean;
  isSettings?: boolean;
  moveStick?: boolean;
  bottom?: boolean;
  onRiichiClick?: (id: number, state: boolean) => void;
};

const setScoreSettings = (fromZero: boolean, fullScore: boolean) => {
  const BASE = 30000;

  const formatScore = (score: number | string) => {
    let scoreNum = typeof score === "number" ? score : parseInt(score);

    if (fromZero) {
      scoreNum = scoreNum - BASE;
    }
    return fullScore ? scoreNum.toString() : (scoreNum / 1000).toFixed(1);
  };

  return formatScore;
};

const Player = observer(
  ({
    player,
    isPanel,
    rightSide,
    riichiStick,
    riichiBtn,
    isSettings,
    bottom,
    onRiichiClick,
  }: Props) => {
    const store = useStore();
    const { gameSettings, riichiCurrent } = store;

    const { id, wind, name, score } = player;
    const side = rightSide ? "right" : "left";
    const cnSettings = isSettings ? styles.settings : "";

    const panel = isPanel ? "panel" : "";
    const fromZero = gameSettings.getVisual()
      ? gameSettings.getVisual().getFromZero()
      : false;
    const fullScore = gameSettings.getVisual()
      ? gameSettings.getVisual().getFullScore()
      : false;
    const riichi = gameSettings.getRound()
      ? gameSettings.getRound().getRiichiPlayerIdsList().includes(id)
      : false;
    const riichiMoveStyle =
      riichiCurrent.getPlayerId() === id && riichiCurrent.getState() === true
        ? rightSide
          ? "rtl"
          : "ltr"
        : "";

    const formatScore = setScoreSettings(fromZero, fullScore);

    return (
      <div className={styles[side]}>
        {riichi && riichiStick && bottom && (
          <RiichiStick moveStyle={riichiMoveStyle}></RiichiStick>
        )}
        <div className={styles.container}>
          <div className={styles.buttons}>
            {riichiBtn && (
              <RiichiButton
                riichi={riichi}
                onClick={() => {
                  if (onRiichiClick) {
                    onRiichiClick(id, !riichi);
                  }
                }}
              ></RiichiButton>
            )}
          </div>
          <div className={`${panel} ${styles.player} ${cnSettings}`}>
            <div className={`${styles['wind'+wind]}`}>
              {name}
            </div>
            <div className={styles.score}>{formatScore(score)}</div>
          </div>
        </div>
        {riichi && riichiStick && !bottom && (
          <RiichiStick moveStyle={riichiMoveStyle}></RiichiStick>
        )}
      </div>
    );
  }
);

export default Player;
