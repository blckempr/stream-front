import React, { useState } from "react";
import { showResults } from "data";
import { SettingsVisual, GameInfo } from "genproto/stream/stream_pb";
import { PlayerType } from "types";
import Player from "components/player/player";
import { getListGames } from "controllers/grpc-reqs";
import { useTranslation } from "react-i18next";

import styles from "components/settings-form/settings-form.module.css";

type IProps = {
  players: PlayerType[];
  settings: SettingsVisual.AsObject;
  CurrentHash: string;
  setSetting: (key: keyof SettingsVisual, value: boolean | number) => void;
  setHash: (hash: string) => void;
};

const SettingsForm: React.FC<IProps> = ({
  players,
  settings,
  setSetting,
  CurrentHash,
  setHash,
}) => {
  const [hash, setTempHash] = useState("");
  const [gamesList, setGamesList] = useState<GameInfo[]>([]);
  const [gamesListVisible, setGamesListVisible] = useState(false);
  const [hashFocused, sethashFocused] = useState(false);

  const { t } = useTranslation();

  let {
    opacity,
    rotation,
    showResultSeconds,
    isIndicators,
    // split, TODO бэк. где сплит?
    fromZero,
    fullScore,
  } = settings;

  const getGamesList = () => {
    getListGames((response: GameInfo[]) => {
      setGamesList(response);
    });
  };

  const makeGamesOptions = (games: GameInfo[]) => {
    let options = games.map((game) => (
      <option key={game.getHash()} value={game.getHash()}>
        {game.getId() + ": " + game.getHash()}
      </option>
    ));
    options.unshift(
      <option disabled hidden value="" key={0}>
        {t("settings.choose-game")}
      </option>
    );
    return options;
  };

  return (
    <div className={styles.settingsForm}>
      <div className={styles.center}>
        {t("settings.top-left")}
        {players.map((player, index) => (
          <div className={styles.centerContainer} key={player.id}>
            <label
              className={styles.label}
              onClick={() => {
                setSetting("setRotation", index);
              }}
            >
              <input
                type="radio"
                checked={rotation === index}
                onChange={() => {
                  setSetting("setRotation", index);
                }}
              />
              <Player player={player} isSettings={true}></Player>
            </label>
          </div>
        ))}
      </div>
      <div className={styles.visual}>
        {t("settings.visual-settings")}
        <section className={styles.visualSection}>
          <select
            className={styles.input}
            defaultValue={(opacity / 100).toFixed(2).toString()}
            onChange={(event) => {
              setSetting("setOpacity", Number(event.target.value) * 100);
            }}
          >
            {Array.from(Array(20).keys()).map((key) => {
              return (
                <option key={key} value={(key * 0.05).toFixed(2).toString()}>
                  {(key * 0.05).toFixed(2).toString()}
                </option>
              );
            })}
          </select>
          {t("settings.transparency")}
        </section>
        <section className={styles.visualSection}>
          <select
            className={styles.input}
            defaultValue={showResultSeconds.toString()}
            onChange={(event) => {
              setSetting("setShowResultSeconds", Number(event.target.value));
            }}
          >
            {Object.keys(showResults).map((key: string) => {
              return (
                <option key={key} value={showResults[key]}>
                  {key}
                </option>
              );
            })}
          </select>
          {t("settings.show-results-sec")}
        </section>
        <section className={styles.visualSection}>
          {t("settings.dora-or-indicator")}
          <label
            className={styles.indRadio}
            onClick={() => {
              setSetting("setIsIndicators", false);
            }}
          >
            <input
              type="radio"
              checked={!isIndicators}
              onChange={() => {
                setSetting("setIsIndicators", false);
              }}
            />
            {t("dora")}
          </label>
          <label
            className={styles.indRadio}
            onClick={() => {
              setSetting("setIsIndicators", true);
            }}
          >
            <input
              type="radio"
              checked={isIndicators}
              onChange={() => {
                setSetting("setIsIndicators", true);
              }}
            />
            {t("indicator")}
          </label>
        </section>
        <section className={styles.visualSection}>
          {t("settings.scores")}
          <label className={styles.pointsSettings}>
            <input
              type="checkbox"
              checked={fromZero}
              onChange={(event) => {
                setSetting("setFromZero", event.target.checked);
              }}
            />
            {t("settings.scores-from-zero")}
          </label>
          <label className={styles.pointsSettings}>
            <input
              type="checkbox"
              checked={fullScore}
              onChange={(event) => {
                setSetting("setFullScore", event.target.checked);
              }}
            />
            {t("settings.scores-full")}
          </label>
        </section>
      </div>
      <div className={styles.hash}>
        {`${t("settings.current-hash")} ${CurrentHash}`}
        <section className={styles.visualSection}>
          <div className={styles.hashGroup}>
            <div
              className={`${styles.hashGroupInput} ${
                hashFocused ? styles.hashGroupInputFocused : ""
              }`}
            >
              <label className={styles.hashLabel}>#</label>
              <input
                className={styles.inputHash}
                type="text"
                size={15}
                value={hash}
                onFocus={() => sethashFocused(true)}
                onBlur={() => sethashFocused(false)}
                onChange={(event) =>
                  setTempHash(
                    event.target.value.replace(/[^-_0-9a-zA-Z]/gi, "")
                  )
                }
              />
            </div>
            <button
              className={styles.button}
              onClick={() => {
                setHash("#" + hash);
                setTempHash("");
              }}
            >
              {t("settings.change-hash")}
            </button>
          </div>
          <div>
            <button
              className={styles.button}
              onClick={() => {
                getGamesList();
                setGamesListVisible(!gamesListVisible);
              }}
            >
              {t("settings.games-list")}
            </button>
          </div>
          {gamesListVisible && (
            <div>
              <select
                defaultValue=''
                className={styles.input}
                onChange={(event) => {
                  setHash(event.target.value);
                  setGamesListVisible(false);
                }}
              >
                {makeGamesOptions(gamesList)}
              </select>
            </div>
          )}
        </section>
      </div>
    </div>
  );
};

export default SettingsForm;
