import React from "react";

import styles from "components/riichi-button/riichi-button.module.css";

type Props = {
  riichi?: boolean;
  onClick: () => void;
};

const RiichiButton = ({ riichi, onClick }: Props) => {
  const cnPressed = riichi ? styles.pressed : styles.unpressed;

  return (
    <div
      className={`${styles.riichiBtn} ${cnPressed}`}
      onClick={onClick}
    ></div>
  );
};

export default RiichiButton;
