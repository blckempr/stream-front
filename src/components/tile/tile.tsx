import React from "react";
import GenTile from "components/tile/tile.gen";

import "components/tile/tile.css";

type Props = {
  index: number;
  gray?: boolean;
  pointer?: boolean;
  aka?: boolean;
  selectable?: boolean;
  onClick?: (e: any) => void;
};

export const Tile = (props: Props) => {
  let cn = "tile ";
  cn += isBack(props.index) ? "tile__back" : "tile__face";
  if (props.gray) {
    cn += " tile__gray";
  }
  if (props.selectable) {
    cn += " tile__selectable";
  }
  if (props.pointer) {
    cn += " tile__pointer";
  }
  let id = tileMapping[props.index];
  return (
    <span className={cn} onClick={props.onClick}>
      {GenTile(id, props.aka)}
    </span>
  );
};

const isBack = (index: number) => {
  return index === 0;
};

const tileMapping: { [key: number]: string } = {
  0: "tile",
  1: "m1",
  2: "m2",
  3: "m3",
  4: "m4",
  5: "m5",
  6: "m6",
  7: "m7",
  8: "m8",
  9: "m9",
  10: "p1",
  11: "p2",
  12: "p3",
  13: "p4",
  14: "p5",
  15: "p6",
  16: "p7",
  17: "p8",
  18: "p9",
  19: "s1",
  20: "s2",
  21: "s3",
  22: "s4",
  23: "s5",
  24: "s6",
  25: "s7",
  26: "s8",
  27: "s9",
  28: "tan",
  29: "nan",
  30: "xia",
  31: "pei",
  32: "haku",
  33: "hatsu",
  34: "chun"
};

export default Tile;
