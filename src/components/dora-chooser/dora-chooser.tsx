import React from "react";
import Tile from "components/tile/tile";
import { useLocalStore, useObserver } from "mobx-react";

type IProps = {
  tileIds: number[];
  setTilesList: (tileIds: number[]) => void;
};

const DoraChooser: React.FC<IProps> = ({ tileIds, setTilesList }) => {
  const tilesState = useLocalStore(() => ({
    clickedTileKey: 0,
    setClickedTileKey(key: number) {
      this.clickedTileKey = key;
    },
    allTilesVisible: false,
    setAllTilesVisible(value: boolean) {
      this.allTilesVisible = value;
    },
  }));

  let tilesWithZero = tileIds.slice();
  tilesWithZero.push(0);

  const numbers = Array.from(Array(35).keys());

  const onClick = (key: number) => {
    tilesState.setAllTilesVisible(!tilesState.allTilesVisible);
    tilesState.setClickedTileKey(key);
  };

  const onPickTile = (id: number) => {
    let newtileIds = tileIds.map((id) => id);

    if (id === 0) {
      newtileIds = newtileIds.filter(
        (value, key) => key !== tilesState.clickedTileKey || value === 0
      );
    } else {
      newtileIds[tilesState.clickedTileKey] = id;
    }

    setTilesList(newtileIds);
    tilesState.setAllTilesVisible(false);
  };

  return useObserver(() => (
    <div className="dora-chooser">
      <div>
        {tilesWithZero.map((id, index) => (
          <Tile
            key={id}
            index={id}
            selectable
            pointer
            onClick={() => onClick(index)}
          />
        ))}
      </div>

      {tilesState.allTilesVisible && (
        <div>
          {numbers.map((i: number) => (
            <Tile
              key={i}
              index={i}
              selectable
              pointer
              onClick={() => onPickTile(i)}
            />
          ))}
        </div>
      )}
    </div>
  ));
};

export default DoraChooser;
