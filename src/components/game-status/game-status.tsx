import React from "react";
import { useStore } from "store/helpers";
import { observer } from "mobx-react";
import { resultsMap } from "types";
import { useTranslation } from "react-i18next";

import styles from "components/game-status/game-status.module.css";

type Props = {
  isPanel?: boolean;
  showResults?: boolean;
};

const GameStatus = observer(({ isPanel, showResults }: Props) => {
  const store = useStore();
  const { game, riichiSum } = store;

  const { t } = useTranslation();

  const offline = false;
  const panel = isPanel ? "panel" : "";

  const round = game.getState() ? game.getState().getRound() : 0;
  const honbaCount = game.getState() ? game.getState().getHonbaCount() : 0;
  const wind = Math.floor(round / 4);
  const resultNum = game.getLastRound() ? game.getLastRound().getOutcome() : -1;

  const getResultText = (result: number) => {
    switch (result) {
      case resultsMap.chombo:
        return t("status.chombo");
      case resultsMap.draw:
        return t("status.draw");
      case resultsMap.multiron:
        return t("status.multiron");
      case resultsMap.ron:
        return t("status.ron");
      case resultsMap.tsumo:
        return t("status.tsumo");
      default:
        return "";
    }
  };

  return (
    <div className={`${panel} ${styles.status}`}>
      {/* //TODO сделать флекс или грид, чтобы убрать из css марджины */}
      <div
        className={`${styles.element} ${styles.round} ${styles['wind'+wind]} ${offline ? styles.offline : ""}`}
      >
        {round % 4}
      </div>
      <span className={`${styles.element} ${styles.renchan}`}>
        {honbaCount}
      </span>
      <span className={`${styles.element} ${styles.riichi}`}>{riichiSum}</span>
      {showResults && (
        <span className={styles.result}>{getResultText(resultNum)}</span>
      )}
    </div>
  );
});

export default GameStatus;
