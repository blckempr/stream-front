import React from "react";

import styles from "components/riichi-stick/riichi-stick.module.css";

type Props = {
  moveStyle: string;
};

const RiichiStick = ({ moveStyle }: Props) => {
  const cnMoveStyle = moveStyle ? styles[moveStyle] : "";

  return (
    <div className={styles.riichiStick}>
      <div className={`${styles.movingBox} ${cnMoveStyle}`}>
        <div className={styles.img}></div>
      </div>
    </div>
  );
};

export default RiichiStick;
