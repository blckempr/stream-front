import React from "react";
import { observer } from "mobx-react";
import { useStore } from "store/helpers";
import GameStatus from "components/game-status/game-status";
import { resultsMap, PlayerType } from "types";
import { WinInfo } from "genproto/stream/stream_pb";
import { winds } from "data";

import styles from "components/results/results.module.css";

const Results = observer(() => {
  const store = useStore();
  const { game, players, playersIdsMap } = store;

  if (!game.getLastRound()) {
    return null;
  }

  const last = game.getLastRound();
  const penaltyId = last.getPenaltyForPlayerId();
  const penaltyPlayer = penaltyId
    ? players.find((p) => p.id === penaltyId)
    : undefined;

  const findBaseValue = (winnerId: number) => {
    const payments = last
      .getPayments()
      .getDirectList()
      .filter((p) => p.getToPlayerId() === winnerId);

    const sum = payments.reduce((acc, v) => {
      return acc + v.getValue();
    }, 0);
    return sum;
  };

  const findLooser = (winnerId: number) => {
    if (
      !(
        last.getOutcome() === resultsMap.ron ||
        last.getOutcome() === resultsMap.multiron
      )
    ) {
      return undefined;
    }

    const directPayment = last
      .getPayments()
      .getDirectList()
      .find((p) => p.getToPlayerId() === winnerId);

    return directPayment
      ? playersIdsMap.get(directPayment.getFromPlayerId())
      : undefined;
  };

  const getTempaiCount = () => {
    if (last.getOutcome() !== resultsMap.draw) {
      return undefined;
    }
    // TODO бэк. в last надо добавить tempai_count
    // var cnt = last.tempai_count;
    // return cnt ? cnt : 0;
    return 0;
  };

  return (
    <div className={styles.results}>
      <div className={`panel ${styles.box}`}>
        <GameStatus showResults></GameStatus>
        {getTempaiCount() === 0 && <div>Все нотен</div>}
        {/* {getTempaiCount() === 4 && <div>Все темпай</div>} */}
        {penaltyPlayer && (
          <section className={`${styles.penalty} ${styles.winner}`}>
            {penaltyPlayer.name}
          </section>
        )}
        <section className={styles.winnersBox}>
          {last.getWinInfosList().map((item: WinInfo, index) => {
            const winner = playersIdsMap
              ? playersIdsMap.get(item.getWinnerPlayerId())
              : null;

            const looser = findLooser(item.getWinnerPlayerId());
            const doraCnt = item.getDoraCount();

            return (
              <div className={styles.win} key={item.getWinnerPlayerId()}>
                <div className={styles.names} key={index}>
                  {winner && (
                    <span className={styles.winner}>{winner.getDisplayName()}</span>
                  )}
                  {looser && (
                    <span className={styles.looser}>{looser.getDisplayName()}</span>
                  )}
                </div>
                <div className={styles.yaku}>
                  {item.getYakuList().map((y) => (
                    <span key={y} className={styles.yaku}>
                      {/* TODO добавить yakuMap с названиями */}
                      {y}
                    </span>
                  ))}
                  {doraCnt && (
                    <span className={styles.dora}>{`дора ${doraCnt}`}</span>
                  )}
                </div>
                <div className={styles.cost}>
                  {item.getHan() > 0 && (
                    <span>
                      {`${item.getHan()} хан`}
                      {item.getFu() > 0 && <span>{` ${item.getFu()} фу`}</span>}
                      .
                    </span>
                  )}

                  {winner && (
                    <span>{` Базовая стоимость ${findBaseValue(
                      winner.getId()
                    )}`}</span>
                  )}
                  {/* TODO при цумо в скобках писать выплаты типа (300/500). Но не понятно, где брать и как хонба */}
                  {/* {limit && <span>{limit}</span>} */}
                  {/* TODO добавить в state лимиты */}
                </div>
              </div>
            );
          })}
        </section>
        <section className={styles.payments}>
          {players.map((player: PlayerType, index: number) => {
            const payment = player.score - player.prevScore;
            const placeDiff = Math.floor(Math.random() * (3 - -3) + -3);
            //TODO бэк. old_places и new_places должны будут приходить с сервера

            //TODO вместо кучи див-ов можно переделать Player на Player(куча дивов) и PlayerContainer
            return (
              <React.Fragment key={player.id}>
                <div className={styles.place}>
                  {index + 1}
                  {placeDiff !== 0 && (
                    <span
                      className={`${styles.diff} ${
                        placeDiff > 0
                          ? styles.diffPositive
                          : styles.diffNegative
                      }`}
                    >
                      {placeDiff}
                    </span>
                  )}
                </div>
                {/* TODO брощенную палку отображать */}
                <div>{`${winds[player.wind]} ${player.name}`}</div>
                <div className={styles.bold}>{player.prevScore}</div>
                {payment !== 0 ? (
                  <div
                    className={`${styles.bold} ${
                      payment > 0
                        ? styles.diffPositive
                        : styles.diffNegative
                    }`}
                  >
                    {payment}
                  </div>
                ) : (
                  <div></div>
                )}
                <div className={styles.bold}>{player.score}</div>
              </React.Fragment>
            );
          })}
        </section>
      </div>
    </div>
  );
});

export default Results;
