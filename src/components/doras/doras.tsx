import React from "react";
import Tile from "components/tile/tile";
import { useTranslation } from "react-i18next";

import styles from "components/doras/doras.module.css";

type Props = {
  tileIds: number[];
  isIndicator?: boolean;
};

const Doras = ({ tileIds, isIndicator }: Props) => {
  const { t } = useTranslation();
  const cnIndicator = isIndicator ? styles.indicator : "";

  return (
    <div className={`panel ${styles.doras}`}>
      <span className={`${styles.verticalBox} ${cnIndicator}`}>
        {isIndicator ? t("indicator") : t("dora")}
      </span>
      <span className={styles.tiles}>
        {tileIds.map((id) => (
          <Tile key={id} index={id} />
        ))}
      </span>
    </div>
  );
};

export default Doras;
