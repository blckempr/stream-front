import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "mobx-react-lite/batchingForReactDom";
import "./i18n";
import App from "App";
import * as serviceWorker from "./serviceWorker";

import "index.css";

//TODO добавить компонент для фолбека, например индикатор загрузки
ReactDOM.render(
  <Suspense fallback={null}>
    <App />
  </Suspense>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
