import { PlayerType } from "types";

export const playersMock: PlayerType[] = [
  {
    id: 0,
    name: "player1",
    score: 0,
    prevScore: 0,
    wind: 0,
  },
  {
    id: 1,
    name: "player2",
    score: 0,
    prevScore: 0,
    wind: 1,
  },
  {
    id: 2,
    name: "player3",
    score: 0,
    prevScore: 0,
    wind: 2,
  },
  {
    id: 3,
    name: "player4",
    prevScore: 0,
    score: 0,
    wind: 3,
  },
];

export const winds: { [key: number]: string } = {
  0: "東",
  1: "南",
  2: "西",
  3: "北",
};

export const scoresMock = [
  [0, 0],
  [1, 0],
  [2, 0],
  [3, 0],
];

export const showResults: { [key: string]: number } = {
  Отключить: 0,
  "5 сек": 5,
  "10 сек": 10,
  "15 сек": 15,
  "20 сек": 20,
  "30 сек": 30,
};
