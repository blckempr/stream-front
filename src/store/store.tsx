import {
  updateVisualSettings,
  updateTilesList,
  updateRiichi,
  updateHash,
} from "controllers/grpc-reqs";
import {
  SettingsVisual,
  GameInfo,
  RiichiState,
  Settings,
  Player,
  StreamResponse,
} from "genproto/stream/stream_pb";
import { playersMock } from "data";
import { PlayerType } from "types";

export function createStore() {
  return {
    game: new GameInfo(),
    players: playersMock as PlayerType[],
    playersIdsMap: new Map<number, Player>(),
    hash: "",
    riichiCurrent: new RiichiState(),
    gameSettings: new Settings(),
    riichiSum: 0,

    myReaction() {
      console.log('reaction', this.game)
    },

    setGame(game: GameInfo) {
      this.game = game;
    },

    setPlayers(players: PlayerType[]) {
      this.players = players;
    },

    setPlayersIdsMap(playersIdsMap: Map<number, Player>) {
      this.playersIdsMap = playersIdsMap;
    },

    setHash(hash: string) {
      this.hash = hash;
    },

    setRiichiCurrent(riichiCurrent: RiichiState) {
      this.riichiCurrent = riichiCurrent;
    },

    setSettings(gameSettings: Settings) {
      this.gameSettings = gameSettings;
    },

    setRiichiSum(riichiSum: number) {
      this.riichiSum = riichiSum;
    },

    get PlayersListWithScores(): PlayerType[] {
      const rawPlayers: Player[] = this.game.getPlayersList();
      const dealerId: number = this.game.getState().getDealerId();
      const scores: Array<Array<
        number
      >> = this.game.getState().getScoresMap().toObject();
      const prevScores: Array<Array<
        number
      >> = this.game.getLastRound().getRoundState().getScoresMap().toObject();
      const dealerKey = rawPlayers.findIndex((p) => p.getId() === dealerId);

      const players = rawPlayers.map((cur: Player, i: number) => {
        const scoreKey = scores.findIndex(
          (s: Array<number>) => s[0] === cur.getId()
        );
        const prevScoreKey = prevScores.findIndex(
          (s: Array<number>) => s[0] === cur.getId()
        );
        const pl: PlayerType = {
          id: cur.getId(),
          name: cur.getDisplayName(),
          score: scoreKey >= 0 ? scores[scoreKey][1] : 0,
          prevScore: prevScoreKey >= 0 ? prevScores[prevScoreKey][1] : 0,
          wind: (i + dealerKey) % 4,
        };
        return pl;
      });

      return players;
    },

    updateState(resp: StreamResponse)  {
      if (resp.getGame()) {
        this.setGame(resp.getGame());
        if (this.game.getPlayersList()) {
          const players: PlayerType[] = this.PlayersListWithScores;

          let mp = new Map<number, Player>();
          this.game.getPlayersList().forEach((p) => {
            mp.set(p.getId(), p);
          });

          this.setPlayers(players);
          this.setPlayersIdsMap(mp);
          // TODO вместо set целого mp сделать playersIdsMap.set(id, value) https://mobx.js.org/refguide/map.html
        }
      }
      if (resp.getHash()) {
        this.setHash(resp.getHash());
      }
      if (resp.getRiichi()) {
        this.setRiichiCurrent(resp.getRiichi());
      }
      if (resp.getSettings()) {
        this.setSettings(resp.getSettings());

        const prevRiichi = resp
          .getSettings()
          .getRound()
          .getRiichiPlayerIdsList().length;
        const curRiichi = this.game.getState()
          ? this.game.getState().getRiichiCount()
          : 0;
        this.setRiichiSum(prevRiichi + curRiichi);
      }
    },

    sendVisualSettings(settings: SettingsVisual) {
      updateVisualSettings(settings);
    },

    sendHash(hash: string) {
      updateHash(hash);
    },

    sendRiichi(riichi: RiichiState) {
      updateRiichi(riichi);
    },

    sendTilesList(tilesList: number[]) {
      updateTilesList(tilesList);
    },
  };
}

export type TStore = ReturnType<typeof createStore>
