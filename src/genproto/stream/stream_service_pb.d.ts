// package: stream
// file: stream/stream_service.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_field_mask_pb from "google-protobuf/google/protobuf/field_mask_pb";
import * as stream_stream_pb from "../stream/stream_pb";

export class ListStreamsRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListStreamsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListStreamsRequest): ListStreamsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListStreamsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListStreamsRequest;
  static deserializeBinaryFromReader(message: ListStreamsRequest, reader: jspb.BinaryReader): ListStreamsRequest;
}

export namespace ListStreamsRequest {
  export type AsObject = {
  }
}

export class ListStreamsResponse extends jspb.Message {
  clearStreamsList(): void;
  getStreamsList(): Array<Stream>;
  setStreamsList(value: Array<Stream>): void;
  addStreams(value?: Stream, index?: number): Stream;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListStreamsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListStreamsResponse): ListStreamsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListStreamsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListStreamsResponse;
  static deserializeBinaryFromReader(message: ListStreamsResponse, reader: jspb.BinaryReader): ListStreamsResponse;
}

export namespace ListStreamsResponse {
  export type AsObject = {
    streamsList: Array<Stream.AsObject>,
  }
}

export class Stream extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Stream.AsObject;
  static toObject(includeInstance: boolean, msg: Stream): Stream.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Stream, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Stream;
  static deserializeBinaryFromReader(message: Stream, reader: jspb.BinaryReader): Stream;
}

export namespace Stream {
  export type AsObject = {
    streamId: string,
    password: string,
  }
}

export class ListGamesRequest extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListGamesRequest): ListGamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGamesRequest;
  static deserializeBinaryFromReader(message: ListGamesRequest, reader: jspb.BinaryReader): ListGamesRequest;
}

export namespace ListGamesRequest {
  export type AsObject = {
    streamId: string,
  }
}

export class ListGamesResponse extends jspb.Message {
  clearGamesList(): void;
  getGamesList(): Array<stream_stream_pb.GameInfo>;
  setGamesList(value: Array<stream_stream_pb.GameInfo>): void;
  addGames(value?: stream_stream_pb.GameInfo, index?: number): stream_stream_pb.GameInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListGamesResponse): ListGamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGamesResponse;
  static deserializeBinaryFromReader(message: ListGamesResponse, reader: jspb.BinaryReader): ListGamesResponse;
}

export namespace ListGamesResponse {
  export type AsObject = {
    gamesList: Array<stream_stream_pb.GameInfo.AsObject>,
  }
}

export class ListenRequest extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListenRequest): ListenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListenRequest;
  static deserializeBinaryFromReader(message: ListenRequest, reader: jspb.BinaryReader): ListenRequest;
}

export namespace ListenRequest {
  export type AsObject = {
    streamId: string,
  }
}

export class GetStateRequest extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetStateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetStateRequest): GetStateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetStateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetStateRequest;
  static deserializeBinaryFromReader(message: GetStateRequest, reader: jspb.BinaryReader): GetStateRequest;
}

export namespace GetStateRequest {
  export type AsObject = {
    streamId: string,
  }
}

export class UpdateHashRequest extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  getHash(): string;
  setHash(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateHashRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateHashRequest): UpdateHashRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateHashRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateHashRequest;
  static deserializeBinaryFromReader(message: UpdateHashRequest, reader: jspb.BinaryReader): UpdateHashRequest;
}

export namespace UpdateHashRequest {
  export type AsObject = {
    streamId: string,
    hash: string,
  }
}

export class UpdateRiichiRequest extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  hasRiichi(): boolean;
  clearRiichi(): void;
  getRiichi(): stream_stream_pb.RiichiState;
  setRiichi(value?: stream_stream_pb.RiichiState): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateRiichiRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateRiichiRequest): UpdateRiichiRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateRiichiRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateRiichiRequest;
  static deserializeBinaryFromReader(message: UpdateRiichiRequest, reader: jspb.BinaryReader): UpdateRiichiRequest;
}

export namespace UpdateRiichiRequest {
  export type AsObject = {
    streamId: string,
    riichi?: stream_stream_pb.RiichiState.AsObject,
  }
}

export class UpdateTilesRequest extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  clearTilesList(): void;
  getTilesList(): Array<number>;
  setTilesList(value: Array<number>): void;
  addTiles(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateTilesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateTilesRequest): UpdateTilesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateTilesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateTilesRequest;
  static deserializeBinaryFromReader(message: UpdateTilesRequest, reader: jspb.BinaryReader): UpdateTilesRequest;
}

export namespace UpdateTilesRequest {
  export type AsObject = {
    streamId: string,
    tilesList: Array<number>,
  }
}

export class UpdateVisualSettingsRequest extends jspb.Message {
  getStreamId(): string;
  setStreamId(value: string): void;

  hasSettings(): boolean;
  clearSettings(): void;
  getSettings(): stream_stream_pb.SettingsVisual;
  setSettings(value?: stream_stream_pb.SettingsVisual): void;

  hasUpdateMask(): boolean;
  clearUpdateMask(): void;
  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateVisualSettingsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateVisualSettingsRequest): UpdateVisualSettingsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateVisualSettingsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateVisualSettingsRequest;
  static deserializeBinaryFromReader(message: UpdateVisualSettingsRequest, reader: jspb.BinaryReader): UpdateVisualSettingsRequest;
}

export namespace UpdateVisualSettingsRequest {
  export type AsObject = {
    streamId: string,
    settings?: stream_stream_pb.SettingsVisual.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

