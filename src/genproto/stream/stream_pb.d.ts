// package: stream
// file: stream/stream.proto

import * as jspb from "google-protobuf";

export class RoundState extends jspb.Message {
  getDealerId(): number;
  setDealerId(value: number): void;

  getRound(): number;
  setRound(value: number): void;

  getRiichiCount(): number;
  setRiichiCount(value: number): void;

  getHonbaCount(): number;
  setHonbaCount(value: number): void;

  getScoresMap(): jspb.Map<number, number>;
  clearScoresMap(): void;
  getFinished(): boolean;
  setFinished(value: boolean): void;

  getYellowZoneAlreadyPlayed(): boolean;
  setYellowZoneAlreadyPlayed(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoundState.AsObject;
  static toObject(includeInstance: boolean, msg: RoundState): RoundState.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RoundState, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoundState;
  static deserializeBinaryFromReader(message: RoundState, reader: jspb.BinaryReader): RoundState;
}

export namespace RoundState {
  export type AsObject = {
    dealerId: number,
    round: number,
    riichiCount: number,
    honbaCount: number,
    scoresMap: Array<[number, number]>,
    finished: boolean,
    yellowZoneAlreadyPlayed: boolean,
  }
}

export class RoundResult extends jspb.Message {
  getOutcome(): OutcomeMap[keyof OutcomeMap];
  setOutcome(value: OutcomeMap[keyof OutcomeMap]): void;

  getPenaltyForPlayerId(): number;
  setPenaltyForPlayerId(value: number): void;

  hasRoundState(): boolean;
  clearRoundState(): void;
  getRoundState(): RoundState;
  setRoundState(value?: RoundState): void;

  clearWinInfosList(): void;
  getWinInfosList(): Array<WinInfo>;
  setWinInfosList(value: Array<WinInfo>): void;
  addWinInfos(value?: WinInfo, index?: number): WinInfo;

  hasPayments(): boolean;
  clearPayments(): void;
  getPayments(): Payments;
  setPayments(value?: Payments): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoundResult.AsObject;
  static toObject(includeInstance: boolean, msg: RoundResult): RoundResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RoundResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoundResult;
  static deserializeBinaryFromReader(message: RoundResult, reader: jspb.BinaryReader): RoundResult;
}

export namespace RoundResult {
  export type AsObject = {
    outcome: OutcomeMap[keyof OutcomeMap],
    penaltyForPlayerId: number,
    roundState?: RoundState.AsObject,
    winInfosList: Array<WinInfo.AsObject>,
    payments?: Payments.AsObject,
  }
}

export class WinInfo extends jspb.Message {
  getWinnerPlayerId(): number;
  setWinnerPlayerId(value: number): void;

  getHan(): number;
  setHan(value: number): void;

  getFu(): number;
  setFu(value: number): void;

  getDoraCount(): number;
  setDoraCount(value: number): void;

  getOpenHand(): boolean;
  setOpenHand(value: boolean): void;

  clearYakuList(): void;
  getYakuList(): Array<number>;
  setYakuList(value: Array<number>): void;
  addYaku(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WinInfo.AsObject;
  static toObject(includeInstance: boolean, msg: WinInfo): WinInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WinInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WinInfo;
  static deserializeBinaryFromReader(message: WinInfo, reader: jspb.BinaryReader): WinInfo;
}

export namespace WinInfo {
  export type AsObject = {
    winnerPlayerId: number,
    han: number,
    fu: number,
    doraCount: number,
    openHand: boolean,
    yakuList: Array<number>,
  }
}

export class Payments extends jspb.Message {
  clearDirectList(): void;
  getDirectList(): Array<Payment>;
  setDirectList(value: Array<Payment>): void;
  addDirect(value?: Payment, index?: number): Payment;

  clearRiichiList(): void;
  getRiichiList(): Array<Payment>;
  setRiichiList(value: Array<Payment>): void;
  addRiichi(value?: Payment, index?: number): Payment;

  clearHonbaList(): void;
  getHonbaList(): Array<Payment>;
  setHonbaList(value: Array<Payment>): void;
  addHonba(value?: Payment, index?: number): Payment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Payments.AsObject;
  static toObject(includeInstance: boolean, msg: Payments): Payments.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Payments, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Payments;
  static deserializeBinaryFromReader(message: Payments, reader: jspb.BinaryReader): Payments;
}

export namespace Payments {
  export type AsObject = {
    directList: Array<Payment.AsObject>,
    riichiList: Array<Payment.AsObject>,
    honbaList: Array<Payment.AsObject>,
  }
}

export class Payment extends jspb.Message {
  getFromPlayerId(): number;
  setFromPlayerId(value: number): void;

  getToPlayerId(): number;
  setToPlayerId(value: number): void;

  getValue(): number;
  setValue(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Payment.AsObject;
  static toObject(includeInstance: boolean, msg: Payment): Payment.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Payment, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Payment;
  static deserializeBinaryFromReader(message: Payment, reader: jspb.BinaryReader): Payment;
}

export namespace Payment {
  export type AsObject = {
    fromPlayerId: number,
    toPlayerId: number,
    value: number,
  }
}

export class Player extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getDisplayName(): string;
  setDisplayName(value: string): void;

  getIdent(): string;
  setIdent(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Player.AsObject;
  static toObject(includeInstance: boolean, msg: Player): Player.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Player, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Player;
  static deserializeBinaryFromReader(message: Player, reader: jspb.BinaryReader): Player;
}

export namespace Player {
  export type AsObject = {
    id: number,
    displayName: string,
    ident: string,
  }
}

export class TableState extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  getHash(): string;
  setHash(value: string): void;

  clearPlayersList(): void;
  getPlayersList(): Array<Player>;
  setPlayersList(value: Array<Player>): void;
  addPlayers(value?: Player, index?: number): Player;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TableState.AsObject;
  static toObject(includeInstance: boolean, msg: TableState): TableState.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TableState, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TableState;
  static deserializeBinaryFromReader(message: TableState, reader: jspb.BinaryReader): TableState;
}

export namespace TableState {
  export type AsObject = {
    status: string,
    hash: string,
    playersList: Array<Player.AsObject>,
  }
}

export class Settings extends jspb.Message {
  hasVisual(): boolean;
  clearVisual(): void;
  getVisual(): SettingsVisual;
  setVisual(value?: SettingsVisual): void;

  hasRound(): boolean;
  clearRound(): void;
  getRound(): SettingsRound;
  setRound(value?: SettingsRound): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Settings.AsObject;
  static toObject(includeInstance: boolean, msg: Settings): Settings.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Settings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Settings;
  static deserializeBinaryFromReader(message: Settings, reader: jspb.BinaryReader): Settings;
}

export namespace Settings {
  export type AsObject = {
    visual?: SettingsVisual.AsObject,
    round?: SettingsRound.AsObject,
  }
}

export class SettingsVisual extends jspb.Message {
  getRotation(): number;
  setRotation(value: number): void;

  getFromZero(): boolean;
  setFromZero(value: boolean): void;

  getFullScore(): boolean;
  setFullScore(value: boolean): void;

  getMirror(): boolean;
  setMirror(value: boolean): void;

  getIsIndicators(): boolean;
  setIsIndicators(value: boolean): void;

  getOpacity(): number;
  setOpacity(value: number): void;

  getShowResults(): boolean;
  setShowResults(value: boolean): void;

  getShowResultSeconds(): number;
  setShowResultSeconds(value: number): void;

  getEventId(): number;
  setEventId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SettingsVisual.AsObject;
  static toObject(includeInstance: boolean, msg: SettingsVisual): SettingsVisual.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SettingsVisual, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SettingsVisual;
  static deserializeBinaryFromReader(message: SettingsVisual, reader: jspb.BinaryReader): SettingsVisual;
}

export namespace SettingsVisual {
  export type AsObject = {
    rotation: number,
    fromZero: boolean,
    fullScore: boolean,
    mirror: boolean,
    isIndicators: boolean,
    opacity: number,
    showResults: boolean,
    showResultSeconds: number,
    eventId: number,
  }
}

export class SettingsRound extends jspb.Message {
  clearRiichiPlayerIdsList(): void;
  getRiichiPlayerIdsList(): Array<number>;
  setRiichiPlayerIdsList(value: Array<number>): void;
  addRiichiPlayerIds(value: number, index?: number): number;

  clearTilesList(): void;
  getTilesList(): Array<number>;
  setTilesList(value: Array<number>): void;
  addTiles(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SettingsRound.AsObject;
  static toObject(includeInstance: boolean, msg: SettingsRound): SettingsRound.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SettingsRound, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SettingsRound;
  static deserializeBinaryFromReader(message: SettingsRound, reader: jspb.BinaryReader): SettingsRound;
}

export namespace SettingsRound {
  export type AsObject = {
    riichiPlayerIdsList: Array<number>,
    tilesList: Array<number>,
  }
}

export class RiichiState extends jspb.Message {
  getPlayerId(): number;
  setPlayerId(value: number): void;

  getState(): boolean;
  setState(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RiichiState.AsObject;
  static toObject(includeInstance: boolean, msg: RiichiState): RiichiState.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RiichiState, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RiichiState;
  static deserializeBinaryFromReader(message: RiichiState, reader: jspb.BinaryReader): RiichiState;
}

export namespace RiichiState {
  export type AsObject = {
    playerId: number,
    state: boolean,
  }
}

export class GameInfo extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getHash(): string;
  setHash(value: string): void;

  clearPlayersList(): void;
  getPlayersList(): Array<Player>;
  setPlayersList(value: Array<Player>): void;
  addPlayers(value?: Player, index?: number): Player;

  hasState(): boolean;
  clearState(): void;
  getState(): RoundState;
  setState(value?: RoundState): void;

  getCaption(): string;
  setCaption(value: string): void;

  hasLastRound(): boolean;
  clearLastRound(): void;
  getLastRound(): RoundResult;
  setLastRound(value?: RoundResult): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GameInfo.AsObject;
  static toObject(includeInstance: boolean, msg: GameInfo): GameInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GameInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GameInfo;
  static deserializeBinaryFromReader(message: GameInfo, reader: jspb.BinaryReader): GameInfo;
}

export namespace GameInfo {
  export type AsObject = {
    id: number,
    hash: string,
    playersList: Array<Player.AsObject>,
    state?: RoundState.AsObject,
    caption: string,
    lastRound?: RoundResult.AsObject,
  }
}

export class StreamResponse extends jspb.Message {
  hasSettings(): boolean;
  clearSettings(): void;
  getSettings(): Settings;
  setSettings(value?: Settings): void;

  hasRiichi(): boolean;
  clearRiichi(): void;
  getRiichi(): RiichiState;
  setRiichi(value?: RiichiState): void;

  hasGame(): boolean;
  clearGame(): void;
  getGame(): GameInfo;
  setGame(value?: GameInfo): void;

  getHash(): string;
  setHash(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamResponse.AsObject;
  static toObject(includeInstance: boolean, msg: StreamResponse): StreamResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamResponse;
  static deserializeBinaryFromReader(message: StreamResponse, reader: jspb.BinaryReader): StreamResponse;
}

export namespace StreamResponse {
  export type AsObject = {
    settings?: Settings.AsObject,
    riichi?: RiichiState.AsObject,
    game?: GameInfo.AsObject,
    hash: string,
  }
}

export interface OutcomeMap {
  OUTCOME_UNSPECIFIED: 0;
  RON: 1;
  TSUMO: 2;
  MULTIRON: 3;
  DRAW: 4;
}

export const Outcome: OutcomeMap;

