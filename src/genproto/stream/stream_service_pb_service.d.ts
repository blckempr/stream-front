// package: stream
// file: stream/stream_service.proto

import * as stream_stream_service_pb from "../stream/stream_service_pb";
import * as stream_stream_pb from "../stream/stream_pb";
import {grpc} from "@improbable-eng/grpc-web";

type StreamServiceListen = {
  readonly methodName: string;
  readonly service: typeof StreamService;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof stream_stream_service_pb.ListenRequest;
  readonly responseType: typeof stream_stream_pb.StreamResponse;
};

type StreamServiceGetState = {
  readonly methodName: string;
  readonly service: typeof StreamService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stream_stream_service_pb.GetStateRequest;
  readonly responseType: typeof stream_stream_pb.StreamResponse;
};

type StreamServiceUpdateHash = {
  readonly methodName: string;
  readonly service: typeof StreamService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stream_stream_service_pb.UpdateHashRequest;
  readonly responseType: typeof stream_stream_pb.StreamResponse;
};

type StreamServiceUpdateVisualSettings = {
  readonly methodName: string;
  readonly service: typeof StreamService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stream_stream_service_pb.UpdateVisualSettingsRequest;
  readonly responseType: typeof stream_stream_pb.SettingsVisual;
};

type StreamServiceUpdateRiichi = {
  readonly methodName: string;
  readonly service: typeof StreamService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stream_stream_service_pb.UpdateRiichiRequest;
  readonly responseType: typeof stream_stream_pb.SettingsRound;
};

type StreamServiceUpdateTiles = {
  readonly methodName: string;
  readonly service: typeof StreamService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stream_stream_service_pb.UpdateTilesRequest;
  readonly responseType: typeof stream_stream_pb.SettingsRound;
};

type StreamServiceListGames = {
  readonly methodName: string;
  readonly service: typeof StreamService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stream_stream_service_pb.ListGamesRequest;
  readonly responseType: typeof stream_stream_service_pb.ListGamesResponse;
};

export class StreamService {
  static readonly serviceName: string;
  static readonly Listen: StreamServiceListen;
  static readonly GetState: StreamServiceGetState;
  static readonly UpdateHash: StreamServiceUpdateHash;
  static readonly UpdateVisualSettings: StreamServiceUpdateVisualSettings;
  static readonly UpdateRiichi: StreamServiceUpdateRiichi;
  static readonly UpdateTiles: StreamServiceUpdateTiles;
  static readonly ListGames: StreamServiceListGames;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class StreamServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listen(requestMessage: stream_stream_service_pb.ListenRequest, metadata?: grpc.Metadata): ResponseStream<stream_stream_pb.StreamResponse>;
  getState(
    requestMessage: stream_stream_service_pb.GetStateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.StreamResponse|null) => void
  ): UnaryResponse;
  getState(
    requestMessage: stream_stream_service_pb.GetStateRequest,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.StreamResponse|null) => void
  ): UnaryResponse;
  updateHash(
    requestMessage: stream_stream_service_pb.UpdateHashRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.StreamResponse|null) => void
  ): UnaryResponse;
  updateHash(
    requestMessage: stream_stream_service_pb.UpdateHashRequest,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.StreamResponse|null) => void
  ): UnaryResponse;
  updateVisualSettings(
    requestMessage: stream_stream_service_pb.UpdateVisualSettingsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.SettingsVisual|null) => void
  ): UnaryResponse;
  updateVisualSettings(
    requestMessage: stream_stream_service_pb.UpdateVisualSettingsRequest,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.SettingsVisual|null) => void
  ): UnaryResponse;
  updateRiichi(
    requestMessage: stream_stream_service_pb.UpdateRiichiRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.SettingsRound|null) => void
  ): UnaryResponse;
  updateRiichi(
    requestMessage: stream_stream_service_pb.UpdateRiichiRequest,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.SettingsRound|null) => void
  ): UnaryResponse;
  updateTiles(
    requestMessage: stream_stream_service_pb.UpdateTilesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.SettingsRound|null) => void
  ): UnaryResponse;
  updateTiles(
    requestMessage: stream_stream_service_pb.UpdateTilesRequest,
    callback: (error: ServiceError|null, responseMessage: stream_stream_pb.SettingsRound|null) => void
  ): UnaryResponse;
  listGames(
    requestMessage: stream_stream_service_pb.ListGamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stream_stream_service_pb.ListGamesResponse|null) => void
  ): UnaryResponse;
  listGames(
    requestMessage: stream_stream_service_pb.ListGamesRequest,
    callback: (error: ServiceError|null, responseMessage: stream_stream_service_pb.ListGamesResponse|null) => void
  ): UnaryResponse;
}

