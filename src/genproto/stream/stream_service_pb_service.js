/* eslint-disable */
//@ts-nocheck
// package: stream
// file: stream/stream_service.proto

var stream_stream_service_pb = require("../stream/stream_service_pb");
var stream_stream_pb = require("../stream/stream_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var StreamService = (function () {
  function StreamService() {}
  StreamService.serviceName = "stream.StreamService";
  return StreamService;
}());

StreamService.Listen = {
  methodName: "Listen",
  service: StreamService,
  requestStream: false,
  responseStream: true,
  requestType: stream_stream_service_pb.ListenRequest,
  responseType: stream_stream_pb.StreamResponse
};

StreamService.GetState = {
  methodName: "GetState",
  service: StreamService,
  requestStream: false,
  responseStream: false,
  requestType: stream_stream_service_pb.GetStateRequest,
  responseType: stream_stream_pb.StreamResponse
};

StreamService.UpdateHash = {
  methodName: "UpdateHash",
  service: StreamService,
  requestStream: false,
  responseStream: false,
  requestType: stream_stream_service_pb.UpdateHashRequest,
  responseType: stream_stream_pb.StreamResponse
};

StreamService.UpdateVisualSettings = {
  methodName: "UpdateVisualSettings",
  service: StreamService,
  requestStream: false,
  responseStream: false,
  requestType: stream_stream_service_pb.UpdateVisualSettingsRequest,
  responseType: stream_stream_pb.SettingsVisual
};

StreamService.UpdateRiichi = {
  methodName: "UpdateRiichi",
  service: StreamService,
  requestStream: false,
  responseStream: false,
  requestType: stream_stream_service_pb.UpdateRiichiRequest,
  responseType: stream_stream_pb.SettingsRound
};

StreamService.UpdateTiles = {
  methodName: "UpdateTiles",
  service: StreamService,
  requestStream: false,
  responseStream: false,
  requestType: stream_stream_service_pb.UpdateTilesRequest,
  responseType: stream_stream_pb.SettingsRound
};

StreamService.ListGames = {
  methodName: "ListGames",
  service: StreamService,
  requestStream: false,
  responseStream: false,
  requestType: stream_stream_service_pb.ListGamesRequest,
  responseType: stream_stream_service_pb.ListGamesResponse
};

exports.StreamService = StreamService;

function StreamServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

StreamServiceClient.prototype.listen = function listen(requestMessage, metadata) {
  var listeners = {
    data: [],
    end: [],
    status: []
  };
  var client = grpc.invoke(StreamService.Listen, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onMessage: function (responseMessage) {
      listeners.data.forEach(function (handler) {
        handler(responseMessage);
      });
    },
    onEnd: function (status, statusMessage, trailers) {
      listeners.status.forEach(function (handler) {
        handler({ code: status, details: statusMessage, metadata: trailers });
      });
      listeners.end.forEach(function (handler) {
        handler({ code: status, details: statusMessage, metadata: trailers });
      });
      listeners = null;
    }
  });
  return {
    on: function (type, handler) {
      listeners[type].push(handler);
      return this;
    },
    cancel: function () {
      listeners = null;
      client.close();
    }
  };
};

StreamServiceClient.prototype.getState = function getState(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(StreamService.GetState, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

StreamServiceClient.prototype.updateHash = function updateHash(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(StreamService.UpdateHash, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

StreamServiceClient.prototype.updateVisualSettings = function updateVisualSettings(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(StreamService.UpdateVisualSettings, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

StreamServiceClient.prototype.updateRiichi = function updateRiichi(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(StreamService.UpdateRiichi, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

StreamServiceClient.prototype.updateTiles = function updateTiles(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(StreamService.UpdateTiles, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

StreamServiceClient.prototype.listGames = function listGames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(StreamService.ListGames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.StreamServiceClient = StreamServiceClient;

