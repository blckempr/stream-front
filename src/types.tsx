export type PlayerType = {
  id: number;
  name: string;
  score: number;
  prevScore: number;
  wind: number;
};

export type resultsType = {
  chombo: number;
  ron: number;
  tsumo: number;
  draw: number;
  multiron: number;
};

export const resultsMap = {
  chombo: 0,
  ron: 1,
  tsumo: 2,
  multiron: 3,
  draw: 4
};
