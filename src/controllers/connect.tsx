import React, { ReactNode, useEffect } from "react";
import { observer } from "mobx-react";
import { queryState } from "controllers/grpc-reqs";
import { useStore } from "store/helpers";

interface IProps {
  children: ReactNode;
}

export const Connect = observer(({ children }: IProps) => {
  const store = useStore();
  const { updateState } = store;

  useEffect(() => {
    queryState(updateState);
  });

  return <div>{children}</div>;
});
