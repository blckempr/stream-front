import { grpc } from "@improbable-eng/grpc-web";
import { StreamService } from "genproto/stream/stream_service_pb_service";
import {
  StreamResponse,
  SettingsVisual,
  RiichiState,
  GameInfo
} from "genproto/stream/stream_pb";
import {
  GetStateRequest,
  ListGamesRequest,
  ListGamesResponse,
  UpdateHashRequest,
  UpdateVisualSettingsRequest,
  UpdateTilesRequest,
  ListenRequest,
  UpdateRiichiRequest
} from "genproto/stream/stream_service_pb";

export function getListGames(f?: (resp: GameInfo[]) => void) {
  const req = new ListGamesRequest();
  req.setStreamId("test1");
  grpc.unary(StreamService.ListGames, {
    request: req,
    host: "http://localhost:9095",
    onEnd: res => {
      const { status, message } = res;
      const resp: ListGamesResponse = message as ListGamesResponse;
      if (status === grpc.Code.OK && message) {
        resp.getGamesList().forEach(e => {
          console.log(e.toObject());
        });
        if (f) {
          f(resp.getGamesList());
        }
      }
    }
  });
}

export function getState(f: (resp: StreamResponse) => void) {
  const req = new GetStateRequest();
  req.setStreamId("test1");
  grpc.unary(StreamService.GetState, {
    request: req,
    host: "http://localhost:9095",
    onEnd: res => {
      const { message, status } = res;
      //TODO проверить, что  status === grpc.Code.OK
      const resp: StreamResponse = message as StreamResponse;
      console.log("getstate", resp.toObject());
      if (f) {
        f(resp);
      }
    }
  });
}

export function queryState(f: (resp: StreamResponse) => void) {
  const req = new ListenRequest();
  req.setStreamId("test1");
  const client = grpc.client(StreamService.Listen, {
    host: "http://localhost:9095"
  });
  client.onMessage(message => {
    const resp: StreamResponse = message as StreamResponse;
    console.log("queryStream.onMessage", resp.toObject());
    if (f) {
      f(resp);
    }
  });
  client.onEnd((code: grpc.Code, msg: string, trailers: grpc.Metadata) => {
    console.log("queryStream.onEnd", code, msg, trailers);
  });
  client.start();
  client.send(req);
}

export function updateHash(input: string) {
  const req = new UpdateHashRequest();
  req.setStreamId("test1");
  req.setHash(input);
  grpc.unary(StreamService.UpdateHash, {
    request: req,
    host: "http://localhost:9095",
    // onEnd: message => {
    //   const resp: StreamResponse = message.message as StreamResponse;
    //   console.log("!!!! updateHash.onEnd message", resp.toObject());
    // }
    onEnd: resp => {
      const { message,status,statusMessage } = resp;
      if (resp) {
        // if (message) { console.log("updateHash.onEnd message", message.toObject()); }
        console.log("updateHash.onEnd status", status);
        // console.log("updateHash.onEnd statusMessage", statusMessage);
      }
    }
  });
}

export function updateVisualSettings(
  input: SettingsVisual,
) {
  const req = new UpdateVisualSettingsRequest();
  req.setStreamId("test1");
  req.setSettings(input);
  grpc.unary(StreamService.UpdateVisualSettings, {
    request: req,
    host: "http://localhost:9095",
    onEnd: res => {
      const { message, status } = res;
      //TODO проверить, что  status === grpc.Code.OK
      const resp: SettingsVisual = message as SettingsVisual;
      console.log("UpdateVisualSettings", resp.toObject());
    }
  });
}

export function updateRiichi(input: RiichiState) {
  const req = new UpdateRiichiRequest();
  req.setStreamId("test1");
  req.setRiichi(input);

  grpc.unary(StreamService.UpdateRiichi, {
    request: req,
    host: "http://localhost:9095",
    onEnd: resp => {
      const { message } = resp;
      if (message) {
        console.log("updateRiichiIds", message.toObject());
      }
    }
  });
}

export function updateTilesList(input: number[]) {
  const req = new UpdateTilesRequest();
  req.setStreamId("test1");
  req.setTilesList(input);

  grpc.unary(StreamService.UpdateTiles, {
    request: req,
    host: "http://localhost:9095",
    onEnd: resp => {
      const { message } = resp;
      if (message) {
        console.log("updateTilesList", message.toObject());
      }
    }
  });
}
