import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import GameLayoutPage from "pages/game-layout/game-layout";
import GameSettingsPage from "pages/game-settings/game-settings";
import TestPage from "pages/test";
import { Connect } from "controllers/connect";
import { StoreProvider } from "store/helpers";

class App extends Component {
  render() {
    return (
      <StoreProvider>
        <Connect>
          <BrowserRouter>
            <Switch>
              <Route path="/settings" component={GameSettingsPage}></Route>
              <Route path="/test/:id" component={TestPage}></Route>
              <Route path="/test" component={TestPage}></Route>
              <Route path="/" component={GameLayoutPage}></Route>
            </Switch>
          </BrowserRouter>
        </Connect>
      </StoreProvider>
    );
  }
}

export default App;
