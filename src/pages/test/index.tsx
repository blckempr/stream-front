import React from "react";
import { withRouter, RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";

import "pages/test/test.css";

interface MatchParams {
  id: string;
}

const getContent = (url: string, params: MatchParams) => {
  return (
    <div className="test-main">
      <p>current url: {url}</p>
      <p>route params: {params.id}</p>
    </div>
  );
};

const TestPage = ({ match }: RouteComponentProps<MatchParams>) => {
  return (
    <section>
      <div className="test-navigation">
        <p>Availiable test pages:</p>
        <Link to="/test/some-test-page">some test page</Link>
      </div>
      {getContent(match.url, match.params)}
    </section>
  );
};

export default withRouter(TestPage);
