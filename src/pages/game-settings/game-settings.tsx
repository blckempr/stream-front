import React from "react";
import GameStatus from "components/game-status/game-status";
import DoraChooser from "components/dora-chooser/dora-chooser";
import Player from "components/player/player";
import SettingsForm from "components/settings-form/settings-form";
import { SettingsVisual, RiichiState } from "genproto/stream/stream_pb";
import { useTranslation } from "react-i18next";
import { useStore } from "store/helpers";
import { observer, useLocalStore } from "mobx-react";

import styles from "pages/game-settings/game-settings.module.css";

const GameSettingsPage: React.FC = observer(() => {
  const store = useStore();
  const {
    gameSettings,
    players,
    hash,
    sendVisualSettings,
    sendTilesList,
    sendRiichi,
    sendHash,
  } = store;

  const settingsFormState = useLocalStore(() => ({
    visible: false,
    setVisible(value: boolean) {
      this.visible = value;
    },
  }));

  const { t } = useTranslation();

  const showResults = gameSettings.getVisual()
    ? gameSettings.getVisual().getShowResults()
    : false;
  const tileIds = gameSettings.getRound()
    ? gameSettings.getRound().getTilesList()
    : [];

  const setSetting = (key: keyof SettingsVisual, value: boolean | number) => {
    let newSettings = gameSettings.getVisual();
    let a: (value: any) => void = newSettings[key];
    a.call(newSettings, value);
    sendVisualSettings(newSettings);
  };

  const onRiichiClick = (id: number, state: boolean) => {
    let riichi = new RiichiState();
    riichi.setPlayerId(id);
    riichi.setState(state);
    sendRiichi(riichi);
  };

  return (
    <div className={styles.container}>
      <div className={styles.settings}>
        <GameStatus></GameStatus>
        {/* TODO добавлять в url /settings если открыта вкладка с сеттингами */}
        <div className={styles.menu}>
          <div
            className={`${styles.menuItem} ${!settingsFormState.visible ? styles.menuItemActive: '' }`}
          >
            <label
              className={styles.menuLabel}
              onClick={() => {
                settingsFormState.setVisible(false);
              }}
            >
              <input
                className={styles.menuRadio}
                type="radio"
                checked={!settingsFormState.visible}
                onChange={() => {
                  settingsFormState.setVisible(false);
                }}
              />
              {t("settings.game")}
            </label>
          </div>
          <div
            className={`${styles.menuItem} ${settingsFormState.visible ? styles.menuItemActive: '' }`}
          >
            <label
              className={styles.menuLabel}
              onClick={() => {
                settingsFormState.setVisible(true);
              }}
            >
              <input
                className={styles.menuRadio}
                type="radio"
                checked={settingsFormState.visible}
                onChange={() => {
                  settingsFormState.setVisible(true);
                }}
              />
              {t("settings.settings")}
            </label>
          </div>
        </div>
        {settingsFormState.visible && (
          <SettingsForm
            settings={gameSettings.getVisual().toObject()}
            setSetting={setSetting}
            CurrentHash={hash}
            setHash={sendHash}
            players={players}
          />
        )}
        {!settingsFormState.visible && (
          <div>
            <div className={`${styles.section} ${styles.center}`}>
              {t("settings.show-results")}
              <label className={styles.results}>
                <input
                  className={styles.resultsInput}
                  type="checkbox"
                  checked={showResults}
                  onChange={(event) => {
                    setSetting("setShowResults", event.target.checked);
                  }}
                />
                <span className={styles.resultsSlider}></span>
              </label>
            </div>

            <div className={styles.section}>
              {t("settings.choose-doras")}
              <DoraChooser
                tileIds={tileIds}
                setTilesList={sendTilesList}
              ></DoraChooser>
            </div>

            <div className={styles.section}>
              {t("settings.choose-riichi")}
              {players.map((player) => (
                <Player
                  key={player.id}
                  player={player}
                  riichiBtn
                  onRiichiClick={onRiichiClick}
                  isSettings={true}
                ></Player>
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
});

export default GameSettingsPage;
