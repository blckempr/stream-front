import React from "react";
import Player from "components/player/player";
import GameStatus from "components/game-status/game-status";
import Doras from "components/doras/doras";
import Results from "components/results/results";
import { useStore } from "store/helpers";
import { observer } from "mobx-react";

import styles from "pages/game-layout/game-layout.module.css";
import "pages/game-layout/game-layout-panel.css";

const GameLayoutPage: React.FC = observer(() => {
  const store = useStore();
  const { game, gameSettings, players } = store;

  const tileIds = gameSettings.getRound()
    ? gameSettings.getRound().getTilesList()
    : [];
  const isIndicators = gameSettings.getVisual()
    ? gameSettings.getVisual().getIsIndicators()
    : false;
  const opacity = gameSettings.getVisual()
    ? gameSettings.getVisual().getOpacity()
    : 80;
  const showResults = gameSettings.getVisual()
    ? gameSettings.getVisual().getShowResults()
    : 80;
  const rotation = gameSettings.getVisual()
    ? gameSettings.getVisual().getRotation()
    : 0;
  const lastDealer = game.getLastRound()
    ? game.getLastRound().getRoundState().getDealerId()
    : 0;

  return (
    <div className={`stream opacity${opacity}`}>
      {showResults && lastDealer !== 0 ? (
        <Results></Results>
      ) : (
        <div className={styles.layout}>
          <div className={`${styles.side} ${styles.left}`}>
            <Player
              player={players[(0 + rotation) % 4]}
              riichiStick
              isPanel
            ></Player>
            <Player
              player={players[(1 + rotation) % 4]}
              riichiStick
              isPanel
              bottom
            ></Player>
          </div>

          <div className={styles.center}>
            <GameStatus isPanel></GameStatus>
            {!!tileIds.length && (
              <Doras tileIds={tileIds} isIndicator={isIndicators}></Doras>
            )}
          </div>
          <div className={`${styles.side} ${styles.right}`}>
            <Player
              player={players[(3 + rotation) % 4]}
              riichiStick
              isPanel
              rightSide
            ></Player>
            <Player
              player={players[(2 + rotation) % 4]}
              riichiStick
              rightSide
              isPanel
              bottom
            ></Player>
          </div>
        </div>
      )}
    </div>
  );
});

export default GameLayoutPage;
