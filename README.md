Mahjong layout for streaming games (frontend only).

**How it looks with backend**

Main page that appears while streaming the game.

<kbd>![page layout](readme-pics/layout.jpg?raw=true "layout")</kbd>

Main page after some game event happens.

<kbd>![page layout-results](readme-pics/layout-results.jpg?raw=true "layout-results")</kbd>

Settings page. Common use case: game assistant use it during the game via mobile device.

<kbd>![page settings-game](readme-pics/settings-game.jpg?raw=true "settings-game")</kbd>

Available visual settings for the layout.

<kbd>![page settings-visual](readme-pics/settings-visual.jpg?raw=true "settings-visual")</kbd>
